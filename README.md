
# AWRDiff
## Motivation
Oracle can compare two ASH snapshots and generate an AWR diff report. It's very handy, except 

- it's a tedious process to compare two snapshots between different DB engines
- it's impossible to compare two snapshots between the current DB instance and the one that's already deleted
- there's no easy way to export and archive ASH snapshots to a third-party storage, and the exported dataset is quite large

However, it's very easy to generate and export/store AWR html files. They are lightweight, compress well and can be stored anywhere FFR.

The problem arises when we need to compare 2 AWRs. OEM does not provide any tools that would allow us to upload an AWR HTML and compare it to existing snapshots or another AWR HTML.

There are scripts out there, but I found they are either a bit too difficult to use or don't do a 1:1 comparison or the output is a custom-formatted text.

## Solution
AWRDiff is a java utility, based on [POJODiff](https://gitlab.com/netikras/pojodiff), and runs on java 8+. It can accept 2 AWR html files and compare them together. As a result, is creates a diff file, using the same AWR HTML format. In addition, since it's a diff, the utility adds **diff** and **diff(%)** columns to most of numeric values in tables. Also, additions and removals are color-coded (red - removed, green - added).

To make it easier to navigate in the diff file, the AWR html javascript is enhanced to allow sorting tables by columns: just click on a column and that table will be sorted by it in ASC order; click again -- it will be resorted DESC.

## Download

### Prebuilt jar
From here: [AWRDiff packages](https://gitlab.com/netikras/awrdiff/-/packages). Whichever version you like. Preferrably, the latest one

### From sources
```
git clone https://gitlab.com/netikras/awrdiff.git
cd awrdiff
./gradlew build jar
```

If the build is successful, the built _jar_ will be placed under `./build/libs/awrdiff.jar`

## Usage
```
java -jar awrdiff.jar -a diff /tmp/awr_report_2699_2700.html /tmp/awr_report_2708_2709.html >/tmp/awrdiff_out.html
```

## Output
![output__load_profile.png](https://gitlab.com/netikras/awrdiff/-/raw/main/doc/assets/output__load_profile.png)

## TODO
- fix the JSON output formatter (so AWRs and their diffs could be dumped as JSONs either for storage or further processing)
- retain href links between SQLID and SQLText
- 
