package com.netikras.tools.awrdiff.simple;

import com.netikras.tools.awrdiff.comparator.impl.simple.ColumnComparingRowPicker;
import com.netikras.tools.awrdiff.model.AWRModel.Cell;
import com.netikras.tools.awrdiff.model.AWRModel.Row;
import com.netikras.tools.pojodiff.CollectionDiff.Elements;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Arrays.asList;

public class ColumnComparingRowPickerTest {

    @Test
    @Ignore
    public void name() {
        ColumnComparingRowPicker picker = new ColumnComparingRowPicker(
                toRows(
                        asList("Tom", "Cat", 80, 81, 82, "Garlic"),
                        asList("Jerry", "Mouse", 73, 74, 75, "Yellow"),
                        asList("Batman", "Bat", 67, 51, 52, "The Beatles"),
                        asList("Robin", "Bird", 57, 21, 12, "Drumstick")
                ),
                toRows(
                        asList("Tom", "Cat", 80, 81, 82, "Garlic"),
                        asList("Woody", "Woodpecker", 73, 74, 71, "Red"),
                        asList("Batman", "Bat", 67, 51, 52, "The Beatles"),
                        asList("Robin", "Bird", 57, 21, 12, "Drumstick"),
                        asList("Jocker", "Villain", 57, 27, 17, "none")
                ),
                asList(1, 2)
        );

        for (Elements<Row> rowElements : picker) {
            System.out.println(rowElements);
        }
    }

    private List<Row> toRows(List<Object>... cells) {
        return Stream.of(cells)
                .map(values -> {
                    Row row = new Row();
                    row.setCells(toCells(values));
                    return row;
                })
                .collect(Collectors.toList());
    }

    private List<Cell<?>> toCells(List<?> values) {
        List<Cell<?>> cells = new ArrayList<>(values.size());
        for (Object value : values) {
            cells.add(new Cell<>(value));
        }
        return cells;
//        return values.stream()
//                .map(val -> new Cell(val))
//                .collect(Collectors.toList())
//                ;
    }

}