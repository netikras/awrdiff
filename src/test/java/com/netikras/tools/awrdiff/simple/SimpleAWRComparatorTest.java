package com.netikras.tools.awrdiff.simple;

import com.netikras.tools.awrdiff.comparator.AWRComparator;
import com.netikras.tools.awrdiff.comparator.AWRComparator.ReportDiff;
import com.netikras.tools.awrdiff.comparator.AWRDiffFormatter;
import com.netikras.tools.awrdiff.comparator.impl.simple.SimpleAWRComparator;
import com.netikras.tools.awrdiff.formatter.Payload;
import com.netikras.tools.awrdiff.model.AWRModel;
import com.netikras.tools.awrdiff.parser.impl.AWRHTMLParser;
import org.junit.Ignore;
import org.junit.Test;

import java.io.FileOutputStream;

public class SimpleAWRComparatorTest {

    @Ignore
    @Test
    public void name() throws Exception {
        AWRModel awr1 = new AWRHTMLParser().parse(ClassLoader.getSystemResourceAsStream("samples/awr/awr1.html"));
        AWRModel awr2 = new AWRHTMLParser().parse(ClassLoader.getSystemResourceAsStream("samples/awr/awr2.html"));
        AWRComparator comparator = new SimpleAWRComparator();
        ReportDiff diff = comparator.compare(awr1, awr2);

        ReportDiff reassigned = diff;

        AWRDiffFormatter formatter = new AWRDiffFormatter();
        Payload formattedReport = formatter.format(diff);
        System.out.println(formattedReport.getMime());
        FileOutputStream outputStream = new FileOutputStream("/tmp/awr_diff." + formattedReport.getFormat());
        outputStream.write(formattedReport.getData());
        outputStream.flush();
        outputStream.close();
    }

}