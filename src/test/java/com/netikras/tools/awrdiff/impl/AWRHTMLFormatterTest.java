package com.netikras.tools.awrdiff.impl;

import com.netikras.tools.awrdiff.formatter.Payload;
import com.netikras.tools.awrdiff.formatter.impl.AWRHTMLFormatter;
import com.netikras.tools.awrdiff.model.AWRModel;
import com.netikras.tools.awrdiff.parser.AWRParser;
import com.netikras.tools.awrdiff.parser.impl.AWRHTMLParser;
import org.junit.Ignore;
import org.junit.Test;

import java.io.FileOutputStream;

public class AWRHTMLFormatterTest {

    @Ignore
    @Test
    public void name() throws Exception {
        AWRParser parser = new AWRHTMLParser();
        AWRModel awr = new AWRHTMLParser().parse(ClassLoader.getSystemResourceAsStream("samples/awr/awr1.html"));
        AWRHTMLFormatter formatter = new AWRHTMLFormatter();
        Payload formattedReport = formatter.format(awr);
        System.out.println(formattedReport.getMime());
        FileOutputStream outputStream = new FileOutputStream("/tmp/awr." + formattedReport.getFormat());
        outputStream.write(formattedReport.getData());
        outputStream.flush();
        outputStream.close();
    }
}