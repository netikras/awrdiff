package com.netikras.tools.awrdiff.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netikras.tools.awrdiff.model.AWRModel;
import com.netikras.tools.awrdiff.parser.impl.AWRHTMLParser;
import org.junit.Ignore;
import org.junit.Test;

public class AWRHTMLParserTest {

    @Ignore
    @Test
    public void name() throws Exception {
        AWRModel awr = new AWRHTMLParser().parse(ClassLoader.getSystemResourceAsStream("samples/awr/awr1.html"));
        System.out.println(new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(awr));
    }


}