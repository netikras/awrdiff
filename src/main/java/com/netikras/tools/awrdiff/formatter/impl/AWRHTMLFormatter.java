package com.netikras.tools.awrdiff.formatter.impl;

import com.netikras.tools.awrdiff.formatter.AWRFormatter;
import com.netikras.tools.awrdiff.formatter.Payload;
import com.netikras.tools.awrdiff.model.AWRModel;
import com.netikras.tools.awrdiff.model.AWRModel.*;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

import static java.util.Arrays.asList;

public class AWRHTMLFormatter implements AWRFormatter {
    private final DecimalFormat df;
    private final DecimalFormat pctFormatter;

    public AWRHTMLFormatter() {
        df = new DecimalFormat("#");
        df.setMaximumFractionDigits(20);
        df.setMinimumIntegerDigits(1);
        df.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(Locale.US));

        pctFormatter = new DecimalFormat("#");
        pctFormatter.setMaximumFractionDigits(2);
        pctFormatter.setMinimumIntegerDigits(1);
        pctFormatter.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(Locale.US));
    }

    protected DecimalFormat getDecimalFormat() {
        return df;
    }

    @Override
    public Payload format(AWRModel model) {

        StringBuilder html = new StringBuilder();
        html.append(getMeta(model));
        html.append("<html lang=\"en\">");
        html.append(getHead(model));
        html.append("<body class=\"awr\">");
        for (AWRSection section : model.getSections()) {
            html.append(toHtml(section, 1));
        }
        html.append("</body>");
        html.append("</html>");

        Payload report = new Payload();
        report.setMime("text/html");
        report.setFormat("html");
        report.setData(html.toString().getBytes());
        return report;
    }

    protected String getHead(AWRModel model) {
        return "<head>" +
                "<title>" + getTitle(model) + "</title>" +
                "<style type=\"text/css\">" + String.join("\n", getStyles()) + "</style>" +
                "<script>" + String.join("\n", getScripts()) + "</script>" +
                "</head>";
    }

    protected List<String> getScripts() {
        return asList(
                "function sortTable(tableId, colId) {" +
                        "  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;" +
                        "  table = document.getElementById(tableId);" +
                        "  switching = true;" +
                        "  dir = \"asc\";" +
                        "  while (switching) {" +
                        "    switching = false;" +
                        "    rows = table.rows;" +
                        "    for (i = 1; i < (rows.length - 1); i++) {" +
                        "      shouldSwitch = false;" +
                        "      x = rows[i].getElementsByTagName(\"TD\")[colId];" +
                        "      y = rows[i + 1].getElementsByTagName(\"TD\")[colId];" +
                        "      " +
                        "      if (!x || !y) {" +
                        "        continue;" +
                        "      }" +
                        "      " +
                        "      valueX = Number(x.innerHTML);" +
                        "      valueY = Number(y.innerHTML);" +
                        "      if (isNaN(valueX) || isNaN(valueY)) {" +
                        "        valueX = x.innerHTML.toLowerCase();" +
                        "        valueY = y.innerHTML.toLowerCase();" +
                        "      }" +
                        "      " +
                        "      if (dir == \"asc\") {" +
                        "        if (valueX > valueY) {" +
                        "          shouldSwitch= true;" +
                        "          break;" +
                        "        }" +
                        "      } else if (dir == \"desc\") {" +
                        "        if (valueX < valueY) {" +
                        "          shouldSwitch = true;" +
                        "          break;" +
                        "        }" +
                        "      }" +
                        "    }" +
                        "    if (shouldSwitch) {" +
                        "      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);" +
                        "      switching = true;" +
                        "      switchcount ++;" +
                        "    } else {" +
                        "      if (switchcount == 0 && dir == \"asc\") {" +
                        "        dir = \"desc\";" +
                        "        switching = true;" +
                        "      }" +
                        "    }" +
                        "  }" +
                        "}"
        );
    }

    protected List<String> getStyles() {
        return asList(
                "th {cursor: pointer;}",
                ".awrnum {text-align:left;}",
                "table {min-width:600;}",

                "body.awr {font:bold 10pt Arial,Helvetica,Geneva,sans-serif;color:black; background:White;}",
                "pre.awr  {font:8pt Courier;color:black; background:White;}",
                "pre_sqltext.awr  {white-space: pre-wrap;}",
                "h1.awr   {font:bold 20pt Arial,Helvetica,Geneva,sans-serif;color:#336699;background-color:White;border-bottom:1px solid #cccc99;margin-top:0pt; margin-bottom:0pt;padding:0px 0px 0px 0px;}",
                "h2.awr   {font:bold 18pt Arial,Helvetica,Geneva,sans-serif;color:#336699;background-color:White;margin-top:4pt; margin-bottom:0pt;}",
                "h3.awr {font:bold 16pt Arial,Helvetica,Geneva,sans-serif;color:#336699;background-color:White;margin-top:4pt; margin-bottom:0pt;}",
                "li.awr {font: 8pt Arial,Helvetica,Geneva,sans-serif; color:black; background:White;}",
                "th.awrnobg {font:bold 8pt Arial,Helvetica,Geneva,sans-serif; color:black; background:White;padding-left:4px; padding-right:4px;padding-bottom:2px}",
                "th.awrbg {font:bold 8pt Arial,Helvetica,Geneva,sans-serif; color:White; background:#0066CC;padding-left:4px; padding-right:4px;padding-bottom:2px}",
                "th.awrdifh {font:bold 8pt Arial,Helvetica,Geneva,sans-serif; color:White; background:lightgray;padding-left:4px; padding-right:4px;padding-bottom:2px}",
                "td.awrnc {font:8pt Arial,Helvetica,Geneva,sans-serif;color:black;background:White;vertical-align:top;}",
                "td.awrc    {font:8pt Arial,Helvetica,Geneva,sans-serif;color:black;background:#FFFFCC; vertical-align:top;}",
                "td.awrnclb {font:8pt Arial,Helvetica,Geneva,sans-serif;color:black;background:White;vertical-align:top;border-left: thin solid black;}",
                "td.awrncbb {font:8pt Arial,Helvetica,Geneva,sans-serif;color:black;background:White;vertical-align:top;border-left: thin solid black;border-right: thin solid black;}",
                "td.awrncrb {font:8pt Arial,Helvetica,Geneva,sans-serif;color:black;background:White;vertical-align:top;border-right: thin solid black;}",
                "td.awrcrb    {font:8pt Arial,Helvetica,Geneva,sans-serif;color:black;background:#FFFFCC; vertical-align:top;border-right: thin solid black;}",
                "td.awrclb    {font:8pt Arial,Helvetica,Geneva,sans-serif;color:black;background:#FFFFCC; vertical-align:top;border-left: thin solid black;}",
                "td.awrcbb    {font:8pt Arial,Helvetica,Geneva,sans-serif;color:black;background:#FFFFCC; vertical-align:top;border-left: thin solid black;border-right: thin solid black;}",
                "a.awr {font:bold 8pt Arial,Helvetica,sans-serif;color:#663300; vertical-align:top;margin-top:0pt; margin-bottom:0pt;}",
                "td.awrnct {font:8pt Arial,Helvetica,Geneva,sans-serif;border-top: thin solid black;color:black;background:White;vertical-align:top;}",
                "td.awrct   {font:8pt Arial,Helvetica,Geneva,sans-serif;border-top: thin solid black;color:black;background:#FFFFCC; vertical-align:top;}",
                "td.awrnclbt  {font:8pt Arial,Helvetica,Geneva,sans-serif;color:black;background:White;vertical-align:top;border-top: thin solid black;border-left: thin solid black;}",
                "td.awrncbbt  {font:8pt Arial,Helvetica,Geneva,sans-serif;color:black;background:White;vertical-align:top;border-left: thin solid black;border-right: thin solid black;border-top: thin solid black;}",
                "td.awrncrbt {font:8pt Arial,Helvetica,Geneva,sans-serif;color:black;background:White;vertical-align:top;border-top: thin solid black;border-right: thin solid black;}",
                "td.awrcrbt     {font:8pt Arial,Helvetica,Geneva,sans-serif;color:black;background:#FFFFCC; vertical-align:top;border-top: thin solid black;border-right: thin solid black;}",
                "td.awrclbt     {font:8pt Arial,Helvetica,Geneva,sans-serif;color:black;background:#FFFFCC; vertical-align:top;border-top: thin solid black;border-left: thin solid black;}",
                "td.awrcbbt   {font:8pt Arial,Helvetica,Geneva,sans-serif;color:black;background:#FFFFCC; vertical-align:top;border-top: thin solid black;border-left: thin solid black;border-right: thin solid black;}",
                "table.tdiff {  border_collapse: collapse; }",
                "table.tscl {width: 600;}",
                "table.tscl tbody, table.tscl thead { display: block; }",
                "table.tscl thead tr th {height: 12px;line-height: 12px;}",
                "table.tscl tbody { height: 100px;overflow-y: auto; overflow-x: hidden;}",
                "table.tscl tbody td, thead th {width: 200;}",
                ".hidden   {position:absolute;left:-10000px;top:auto;width:1px;height:1px;overflow:hidden;}",
                ".pad   {margin-left:17px;}",
                ".doublepad {margin-left:34px;}"
        );
    }

    protected String getTitle(AWRModel model) {
        return "AWR report";
    }

    protected String getMeta(AWRModel model) {
        return "<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">";
    }

    protected String toHtml(AWRTable table) {
        StringBuilder html = new StringBuilder();
        html.append("<p></p>");
        String tableId = "" + table.hashCode();
        if (table.getTitle() != null) {
            html.append("<p>").append(table.getTitle()).append("</p>");
        }
        html.append(getTableOpeningHTML(tableId, table.getSummary()));
        html.append("<tbody>");
        html.append(getTableHeadersHTML(tableId, table.getHeader()));

        boolean coloured = false;
        for (Row row : table.getRows()) {
            html.append("<tr>");
            html.append(getCellsHTML(tableId, table.getHeader(), row.getCells(), coloured));
            html.append("</tr>");
            coloured = !coloured;
        }
        html.append("</tbody>");
        html.append("</table>");
        return html.toString();
    }

    protected String getCellsHTML(String tableId, List<HeaderCell> headers, List<Cell<?>> cells, boolean coloured) {
        StringBuilder html = new StringBuilder();
        for (int columnIdx = 0; columnIdx < cells.size(); columnIdx++) {
            Object value = cells.get(columnIdx).getValue();
            HeaderCell headerCell = headers.size() > columnIdx ? headers.get(columnIdx) : new HeaderCell();
            if ("%".equals(headerCell.getUnits()) && value != null && value instanceof Double) {
                value = pctFormatter.format(value);
            } else {
                value = format(value);
            }
            html.append("<td scope=\"row\" class='")
                    .append(coloured ? " awrnc" : " awrc")
                    .append(Boolean.TRUE.equals(headerCell.getNumeric()) ? " awrnum" : " awrtxt")
                    .append("'").append(">")
                    .append(value)
                    .append("</td>");
        }
        return html.toString();
    }

    protected String getTableOpeningHTML(String tableId, String summary) {
        StringBuilder html = new StringBuilder();
        html.append("<table")
                .append(String.format(" id=\"%s\"", tableId))
                .append(" border=\"0\"")
                .append(" class=\"tdiff\"")
                .append(String.format(" summary=\"%s\"", summary))
                .append(">");
        return html.toString();
    }

    protected String getTableHeadersHTML(String tableId, List<HeaderCell> headers) {
        StringBuilder html = new StringBuilder();
        html.append("<tr>");
        for (int colId = 0; colId < headers.size(); colId++) {
            HeaderCell header = headers.get(colId);
            String text = header.getValue();
            if (header.getUnits() != null && !header.getUnits().trim().isEmpty()) {
                if (text == null) {
                    text = header.getUnits();
                } else {
                    String suffix = "(" + header.getUnits() + ")";
                    if (!text.endsWith(suffix)) {
                        text = text + " " + suffix;
                    }
                }
            }
            html.append("<th")
                    .append("diff".equals(header.getValue()) ? " class=\"awrdifh\"" : " class=\"awrbg\"")
                    .append(" scope=\"col\"")
                    .append(String.format(" onclick=\"sortTable(%s, %d)\"", tableId, colId))
                    .append(">")
                    .append(text)
                    .append("</th>");
        }
        html.append("</tr>");
        return html.toString();
    }

    protected String format(Object value) {
        if (value == null) {
            return formatNull();
        } else if (value instanceof String) {
            return formatText((String) value);
        } else if (value instanceof Number) {
            return formatNumber((Number) value);
        } else {
            return formatUnknown(value);
        }
    }

    protected String formatUnknown(Object value) {
        return "?" + value + "?";
    }

    protected String formatNull() {
        return "&nbsp;";
    }

    protected String formatText(String value) {
        return value;
    }

    protected String formatNumber(Number value) {
        String formatted = getDecimalFormat().format(value);
        if (formatted.contains(".")) {
            formatted = formatted
                    .replaceAll("9{4,}\\d$", "9")
                    .replaceAll("0{4,}\\d$", "");
        }
        return formatted;
    }

    protected String toHtml(AWRSection section, int level) {
        StringBuilder html = new StringBuilder();
        html.append("<p></p>");
        html.append("<h").append(level).append(" class=\"awr\">")
                .append(section.getTitle())
                .append("</h").append(level).append(">");
        html.append("<p></p>");
        section.getTables().forEach(table -> html.append(toHtml(table)));
        section.getSubSections().forEach(sub -> html.append(toHtml(sub, level + 1)));

        return html.toString();
    }
}
