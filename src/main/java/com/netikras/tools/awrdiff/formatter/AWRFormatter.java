package com.netikras.tools.awrdiff.formatter;

import com.netikras.tools.awrdiff.model.AWRModel;

public interface AWRFormatter {
    Payload format(AWRModel model);
}
