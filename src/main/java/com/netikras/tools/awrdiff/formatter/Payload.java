package com.netikras.tools.awrdiff.formatter;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Payload {
    private String mime;
    private String format;
    private byte[] data;
}
