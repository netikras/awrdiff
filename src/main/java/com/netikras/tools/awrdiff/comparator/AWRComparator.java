package com.netikras.tools.awrdiff.comparator;

import com.netikras.tools.awrdiff.comparator.impl.simple.ColumnComparingRowPicker;
import com.netikras.tools.awrdiff.model.AWRModel;
import com.netikras.tools.awrdiff.model.AWRModel.*;
import com.netikras.tools.pojodiff.AbstractDiff;
import com.netikras.tools.pojodiff.CollectionDiff;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import static java.util.Arrays.asList;

public interface AWRComparator {

    ReportDiff compare(AWRModel model1, AWRModel model2);

    @Getter
    @Setter
    @ToString(callSuper = true)
    class ReportDiff extends AbstractDiff<AWRModel> {
        public ReportDiff(AWRModel old, AWRModel current) {
            super(old, current);
            addFieldAccessor("sections", AWRModel::getSections, (o, c) -> new CollectionDiff<>(o, c, SectionDiff::new));
        }
    }

    @Getter
    @Setter
    @ToString(callSuper = true)
    class SectionDiff extends AbstractDiff<AWRSection> {
        public SectionDiff(AWRSection old, AWRSection current) {
            super(old, current);
            addFieldAccessor("title", AWRSection::getTitle);
            addFieldAccessor("subSections", AWRSection::getSubSections, (o, c) -> new CollectionDiff<>(o, c, SectionDiff::new));
            addFieldAccessor("tables", AWRSection::getTables, (o, c) -> new CollectionDiff<>(o, c, TableDiff::new));
        }
    }

    @Getter
    @Setter
    @ToString(callSuper = true)
    class TableDiff extends AbstractDiff<AWRTable> {

        private final Map<String, List<Integer>> columnsForComparison = new HashMap<>();

        public TableDiff(AWRTable old, AWRTable current) {
            super(old, current);
            addFieldAccessor("title", AWRTable::getTitle);
            addFieldAccessor("summary", AWRTable::getSummary);
            addFieldAccessor("header", AWRTable::getHeader, (o, c) -> new CollectionDiff<>(o, c, HeaderDiff::new));
            //            addFieldAccessor("rows", AWRTable::getRows, (o, c) -> new CollectionDiff<>(o, c, RowDiff::new));
            addFieldAccessor("rows", AWRTable::getRows, (o, c) -> new CollectionDiff<>(() -> o, () -> c, RowDiff::new,
                    (ol, cu) -> new ColumnComparingRowPicker(ol, cu, getColumnsForComparison())));

            columnsForComparison.put("^Operating System Statistics - Detail$", asList());
            columnsForComparison.put("^Top Process Types by Wait Class$", asList(0, 1, 2));
            columnsForComparison.put("^Top Process Types by CPU Used$", asList(0, 1));
            columnsForComparison.put("^SQL ordered by .*$", asList(indexOfColumn("SQL Id")));
            columnsForComparison.put("^Checkpoint Activity$", asList());
            columnsForComparison.put("^Instance Recovery Stats$", asList());
            columnsForComparison.put("^PGA Aggr Summary$", asList());
            columnsForComparison.put("^PGA Aggr Target Stats$", asList());
            columnsForComparison.put("^Undo Segment Summary$", asList());
            columnsForComparison.put("^Undo Segment Stats$", asList());
            columnsForComparison.put("^Latch Miss Sources$", asList(0, 1));
            columnsForComparison.put("^Mutex Sleep Summary$", asList(0, 1));
            columnsForComparison.put("^Segments by .*$", asList(0, 1, 2));
            columnsForComparison.put("^Process Memory Summary$", asList(0, 1));
            columnsForComparison.put("^SGA breakdown difference by Pool and Name$", asList(0, 1));
            columnsForComparison.put("^Top Blocking Sessions$", asList());
            columnsForComparison.put("^Activity Over Time$", asList());

        }

        private int indexOfColumn(String name) {
            List<HeaderCell> header = getCurrent() != null ? getCurrent().getHeader() : getOld().getHeader();
            for (int i = 0; i < header.size(); i++) {
                if (Objects.equals(header.get(i).getValue(), name)) {
                    return i;
                }
            }
            return 0;
        }

        private List<Integer> getColumnsForComparison() {
            String title = getCurrent() != null ? getCurrent().getTitle() : getOld().getTitle();

            if (title != null) {
                for (Entry<String, List<Integer>> entry : columnsForComparison.entrySet()) {
                    if (title.matches(entry.getKey())) {
                        return entry.getValue();
                    }
                }
            }

            return asList(0); // default -- compare the first column
        }
    }

    @Getter
    @Setter
    @ToString(callSuper = true)
    class RowDiff extends AbstractDiff<Row> {
        public RowDiff(Row old, Row current) {
            super(old, current);
            addFieldAccessor("cells", Row::getCells, (o, c) -> new CollectionDiff<>(o, c, CellDiff::new));
            //            addFieldAccessor("cells", Row::getCells, (o, c) -> new CollectionDiff<>(o, c, CellDiff::new, (oldie, currie) -> new ColumnComparingRowPicker<Row>(oldie, currie, asList(1))));
        }
    }

    @Getter
    @Setter
    @ToString(callSuper = true)
    class CellDiff extends AbstractDiff<Cell<?>> {
        public CellDiff(Cell<?> old, Cell<?> current) {
            super(old, current);
            addFieldAccessor("value", Cell::getValue);
        }
    }

    @Getter
    @Setter
    @ToString(callSuper = true)
    class HeaderDiff extends AbstractDiff<HeaderCell> {
        public HeaderDiff(HeaderCell old, HeaderCell current) {
            super(old, current);
            addFieldAccessor("value", HeaderCell::getValue);
            addFieldAccessor("units", HeaderCell::getUnits);
            addFieldAccessor("numeric", HeaderCell::getNumeric);
        }
    }
}
