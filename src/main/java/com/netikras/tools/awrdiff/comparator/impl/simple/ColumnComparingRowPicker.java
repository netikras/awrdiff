package com.netikras.tools.awrdiff.comparator.impl.simple;

import com.netikras.tools.awrdiff.model.AWRModel.Row;
import com.netikras.tools.pojodiff.CollectionDiff.Elements;
import com.netikras.tools.pojodiff.CollectionDiff.ElementsPicker;

import java.util.*;

public class ColumnComparingRowPicker implements ElementsPicker<Row> {
    private final List<Row> old;
    private final List<Row> current;
    private final List<Integer> columnsToCompare;

    public ColumnComparingRowPicker(Collection<Row> old, Collection<Row> current, List<Integer> columnsToCompare) {
        this(new ArrayList<>(old), new ArrayList<>(current), columnsToCompare);
    }

    public ColumnComparingRowPicker(List<Row> old, List<Row> current, List<Integer> columnsToCompare) {
        this.old = new ArrayList<>(old);
        this.current = new ArrayList<>(current);
        this.columnsToCompare = new ArrayList<>(columnsToCompare);
    }

    @Override
    public boolean hasNext() {
        return old.size() > 0 || current.size() > 0;
    }

    @Override public Elements<Row> next() {
        Elements<Row> pair = new Elements<>();
        if (old.size() > 0) {
            pair.setOld(old.remove(0));
        }

        if (current.size() > 0) {
            getComplementaryRow(pair.getOld()).ifPresent(pair::setCurrent);
        }

        return pair;
    }

    private Optional<Row> getComplementaryRow(Row oldRow) {
        if (current.size() > 0) {
            if (oldRow == null) {
                return Optional.ofNullable(current.remove(0));
            } else {
                List<Object> oldValues = new ArrayList<>(columnsToCompare.size());
                for (Integer colId : columnsToCompare) {
                    if (oldRow.getCells().size() <= colId) {
                        throw new RuntimeException("Column ID " + colId + " exceeds old collection size " + oldRow.getCells().size());
                    } else {
//                        oldValues.add(oldRow.getCells().get(colId).getValue());
                        oldRow.getCells().forEach(cell -> oldValues.add(cell.getValue()));
                    }
                }

                for (int rowIndex = 0; rowIndex < current.size(); rowIndex++) {
                    Row row = current.get(rowIndex);
                    if (rowMatches(row, columnsToCompare, oldValues)) {
                        return Optional.ofNullable(current.remove(rowIndex));
                    }
                }
            }
        }
        return Optional.empty();
    }

    private boolean rowMatches(Row row, List<Integer> colIDs, List<Object> values) {
        for (Integer colId : colIDs) {
            if (row.getCells().size() <= colId) {
                break;
            } else {
                Object oldValue = values.get(colId);
                Object currentValue = row.getCells().get(colId).getValue();
                if (!Objects.equals(oldValue, currentValue)) {
                    return false;
                }
            }
        }
        return true;
    }
}
