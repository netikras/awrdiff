package com.netikras.tools.awrdiff.comparator.impl.simple;

import com.netikras.tools.awrdiff.comparator.AWRComparator;
import com.netikras.tools.awrdiff.model.AWRModel;

public class SimpleAWRComparator implements AWRComparator {

    @Override
    public ReportDiff compare(AWRModel model1, AWRModel model2) {
        ReportDiff diff = new ReportDiff(model1, model2);
        diff.diff();
        return diff;
    }

}
