package com.netikras.tools.awrdiff.comparator;

import com.netikras.tools.awrdiff.comparator.AWRComparator.CellDiff;
import com.netikras.tools.awrdiff.comparator.AWRComparator.HeaderDiff;
import com.netikras.tools.awrdiff.comparator.AWRComparator.ReportDiff;
import com.netikras.tools.awrdiff.comparator.AWRComparator.RowDiff;
import com.netikras.tools.awrdiff.formatter.Payload;
import com.netikras.tools.awrdiff.formatter.impl.AWRHTMLFormatter;
import com.netikras.tools.awrdiff.model.AWRModel.AWRSection;
import com.netikras.tools.awrdiff.model.AWRModel.AWRTable;
import com.netikras.tools.awrdiff.model.AWRModel.Cell;
import com.netikras.tools.awrdiff.model.AWRModel.HeaderCell;
import com.netikras.tools.pojodiff.AbstractDiff;
import com.netikras.tools.pojodiff.ChangedValue;
import com.netikras.tools.pojodiff.CollectionDiff;
import lombok.NonNull;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Optional.ofNullable;

public class AWRDiffFormatter extends AWRHTMLFormatter {

    public AWRDiffFormatter() {

    }

    public Payload format(ReportDiff diff) {
        StringBuilder html = new StringBuilder();
        html.append(getMeta(diff));
        html.append("<html lang=\"en\">");
        html.append(getHead(diff));
        html.append("<body class=\"awr\">");

        CollectionDiff<AWRSection, List<AWRSection>> sections = (CollectionDiff<AWRSection, List<AWRSection>>) diff.getField("sections");
        for (ChangedValue<AWRSection> section : sections.getValues()) {
            html.append(toHtml((AbstractDiff<AWRSection>) section, 1));
        }
        html.append("</body>");
        html.append("</html>");

        Payload report = new Payload();
        report.setMime("text/html");
        report.setFormat("html");
        report.setData(html.toString().getBytes());
        return report;
    }

    private String getMeta(ReportDiff diff) {
        return "<meta/>";
    }

    protected String toHtml(AbstractDiff<AWRSection> section, int level) {
        StringBuilder html = new StringBuilder();
        html.append("<p></p>");
        html.append("<h").append(level).append(" class=\"awr\">")
                .append(toDiffString(section.getField("title"), true))
                .append("</h").append(level).append(">");
        html.append("<p></p>");
        CollectionDiff<AWRTable, List<AWRTable>> tables =
                (CollectionDiff<AWRTable, List<AWRTable>>) section.getField("tables");
        tables.getValues().forEach(table -> html.append(toHtml(table)));
        CollectionDiff<AWRSection, List<AWRSection>> subSections =
                (CollectionDiff<AWRSection, List<AWRSection>>) section.getField("subSections");
        subSections.getValues().forEach(sub -> html.append(toHtml((AbstractDiff<AWRSection>) sub, level + 1)));

        return html.toString();
    }

    private String toHtml(ChangedValue<AWRTable> table) {
        StringBuilder html = new StringBuilder();
        AbstractDiff<AWRTable> tableDiff = (AbstractDiff<AWRTable>) table;
        String tableId = "" + table.hashCode();
        html.append("<p></p>");
        ChangedValue<String> title = (ChangedValue<String>) tableDiff.getField("title");
        if (title != null) {
            html.append("<p class=\"table_title\">").append(toDiffString(title, true)).append("</p>");
        }
        ChangedValue<String> summary = (ChangedValue<String>) tableDiff.getField("summary");
        html.append(getTableOpeningHTML(tableId, toDiffString(summary, false)));
        html.append("<tbody>");

        List<HeaderCell> headerCells = toHeaders((CollectionDiff<HeaderDiff, List<HeaderDiff>>) tableDiff.getField("header"));
        List<HeaderCell> enhancedHeaders = enhance(headerCells);
        html.append(getTableHeadersHTML(tableId, enhancedHeaders));

        boolean coloured = false;
        CollectionDiff<RowDiff, List<RowDiff>> rows = (CollectionDiff<RowDiff, List<RowDiff>>) tableDiff.getField("rows");
        if (rows != null) {
            for (ChangedValue<RowDiff> row : rows.getValues()) {
                AbstractDiff<RowDiff> rowDiff = (AbstractDiff<RowDiff>) row;
                List<Cell<?>> cells = toCells((CollectionDiff<CellDiff, List<CellDiff>>) rowDiff.getField("cells"), headerCells);
                html.append("<tr>");
                html.append(getCellsHTML(tableId, enhancedHeaders, cells, coloured));
                html.append("</tr>");
                coloured = !coloured;
            }
        }
        html.append("</tbody>");
        html.append("</table>");

        return html.toString();
    }

    private List<HeaderCell> enhance(List<HeaderCell> headerCells) {
        List<HeaderCell> enhanced = new ArrayList<>();
        headerCells.forEach(cell -> {
            enhanced.add(cell);
            if (Boolean.TRUE.equals(cell.getNumeric())) {
                HeaderCell diffVal = new HeaderCell();
                diffVal.setValue("diff");
                diffVal.setNumeric(true);
                diffVal.setUnits(null);
                enhanced.add(diffVal);

                HeaderCell diffPct = new HeaderCell();
                diffPct.setValue("diff");
                diffPct.setNumeric(true);
                diffPct.setUnits("%");
                enhanced.add(diffPct);
            }
        });
        return enhanced;
    }

    private List<Cell<?>> toCells(CollectionDiff<CellDiff, List<CellDiff>> cellsDiff, List<HeaderCell> headers) {
        List<Cell<?>> cells = new ArrayList<>();
        if (cellsDiff != null) {
            for (int columnIdx = 0; columnIdx < cellsDiff.getValues().size(); columnIdx++) {
                AbstractDiff<CellDiff> cellDiff = (AbstractDiff<CellDiff>) cellsDiff.getValues().get(columnIdx);
                ChangedValue<?> valueDiff = cellDiff.getField("value");
                String value = toDiffString(valueDiff, true);
                cells.add(new Cell<>(value));

                if (headers != null && headers.size() > columnIdx) {
                    HeaderCell header = headers.get(columnIdx);
                    if (header.getNumeric()) {
                        Double oldValue = ofNullable(valueDiff.getOld())
                                .filter(old -> old instanceof Double)
                                .map(old -> (Double) old)
                                .orElse(0D);

                        Double currentValue = ofNullable(valueDiff.getCurrent())
                                .filter(old -> old instanceof Double)
                                .map(old -> (Double) old)
                                .orElse(0D);

                        int decPlaces = BigDecimal.valueOf(currentValue != 0 ? currentValue : oldValue).scale();
                        BigDecimal diff = BigDecimal.valueOf(currentValue - oldValue).setScale(decPlaces, RoundingMode.HALF_UP);
                        double diffPct;
                        if (oldValue == 0) {
                            if (currentValue == 0) {
                                diffPct = 0;
                            } else {
                                diffPct = 100;
                            }
                        } else {
                            diffPct = (diff.doubleValue() * 100 / oldValue);
                        }

                        cells.add(new Cell<>(diff.doubleValue()));
                        cells.add(new Cell<>(diffPct));
                    }
                }
            }
        }
        return cells;
    }

    private List<HeaderCell> toHeaders(CollectionDiff<HeaderDiff, List<HeaderDiff>> headerDiffs) {
        List<HeaderCell> headers = new ArrayList<>();
        if (headerDiffs != null) {
            for (int colId = 0; colId < headerDiffs.getValues().size(); colId++) {
                AbstractDiff<HeaderDiff> header = (AbstractDiff<HeaderDiff>) headerDiffs.getValues().get(colId);
                boolean numeric = ofNullable(header.getField("numeric")).map(ChangedValue::getCurrent).map(v -> (Boolean) v).orElse(false);
                String units = ofNullable(header.getField("units")).map(ChangedValue::getCurrent).map(v -> (String) v).orElse("");
                HeaderCell cell = new HeaderCell();
                cell.setValue(toDiffString(header.getField("value"), true));
                cell.setNumeric(numeric);
                cell.setUnits(units);
                headers.add(cell);
            }
        }
        return headers;
    }

    protected String format(Object value) {
        if (value == null) {
            return formatNull();
        } else if (value instanceof String) {
            return formatText((String) value);
        } else if (value instanceof Number) {
            return formatNumber((Number) value);
        } else {
            return formatUnknown(value);
        }
    }

    protected String formatUnknown(Object value) {
        return "?" + value + "?";
    }

    protected String formatNull() {
        return "&nbsp;";
    }

    protected String formatText(String value) {
        return value;
    }

    protected String formatNumber(Number value) {
        String formatted = getDecimalFormat().format(value);
        if (formatted.contains(".")) {
            formatted = formatted
                    .replaceAll("9{4,}\\d$", "9")
                    .replaceAll("0{4,}\\d$", "");
        }
        return formatted;
    }

    private @NonNull String toDiffString(ChangedValue<?> diff, boolean span) {
        if (diff != null) {
            if (span) {
                // @formatter:off
                switch (diff.getType()) {
                    case NONE: return "<span class=\"diff_none\">" + format(diff.getCurrent()) + "</span>";
                    case ADDED: return "<span class=\"diff_added\">" + format(diff.getCurrent()) + "</span>";
                    case REMOVED: return  "<span class=\"diff_removed\">" + format(diff.getOld()) + "</span>";
                    case CHANGED: return  "" +
                            "<span class=\"diff_removed\">" + format(diff.getOld()) + "</span>" +
                            "<span class=\"diff_added\">" + format(diff.getCurrent()) + "</span>";
                };
                // @formatter:on
            } else {
                // @formatter:off
                switch (diff.getType()) {
                    case NONE: return format(diff.getCurrent());
                    case ADDED: return format(diff.getCurrent());
                    case REMOVED: return format(diff.getOld());
                    case CHANGED: return  "" + format(diff.getOld()) + "→" + format(diff.getCurrent());
                };
                // @formatter:on
            }
        }
        return "<span class=\"diff_missing\"></span>";
    }

    protected String getHead(ReportDiff model) {
        return "<head>" +
                "<title>" + getTitle(model) + "</title>" +
                "<style type=\"text/css\">" + String.join("\n", getStyles()) + "</style>" +
                "<script>" + String.join("\n", getScripts()) + "</script>" +
                "</head>";
    }

    protected List<String> getStyles() {
        List<String> styles = new ArrayList<>(super.getStyles());
        styles.addAll(asList(
                ".diff_added{background-color:limegreen}",
                ".diff_removed{background-color:darkorange}",
                ".diff_removed{margin-right:2px}"
        ));
        return styles;
    }

    protected List<String> getScripts() {
        return super.getScripts();
    }

    private String getTitle(ReportDiff model) {
        return "AWR diff";
    }
}
