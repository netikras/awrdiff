package com.netikras.tools.awrdiff.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class AWRModel {
    private List<AWRSection> sections;

    @Getter
    @Setter
    @ToString
    public static class AWRSection {
        private String title;
        private List<AWRSection> subSections;
        private List<AWRTable> tables;
    }

    @Getter
    @Setter
    @ToString
    public static class AWRTable {
        private String title;
        private String summary;
        private List<HeaderCell> header;
        private List<Row> rows;

    }

    @Getter
    @Setter
    @ToString
    public static class Row {
        private List<Cell<?>> cells;

    }

    @Getter
    @Setter
    @ToString
    public static class Cell<T> {
        private T value;

        public Cell() {
        }

        public Cell(T value) {
            this.value = value;
        }
    }

    @Getter
    @Setter
    @ToString
    public static class HeaderCell extends Cell<String> {
        private String value;
        private String units;
        private Boolean numeric;
    }
}
