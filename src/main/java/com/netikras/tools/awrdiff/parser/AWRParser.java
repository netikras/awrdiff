package com.netikras.tools.awrdiff.parser;

import com.netikras.tools.awrdiff.model.AWRModel;

import java.io.InputStream;

public interface AWRParser {
    AWRModel parse(InputStream is) throws ParsingException;

    class ParsingException extends Exception {
        public ParsingException() {
            super();
        }

        public ParsingException(String message) {
            super(message);
        }

        public ParsingException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}
