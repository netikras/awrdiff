package com.netikras.tools.awrdiff.parser.impl;

import com.netikras.tools.awrdiff.model.AWRModel;
import com.netikras.tools.awrdiff.model.AWRModel.*;
import com.netikras.tools.awrdiff.parser.AWRParser;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import java.io.InputStream;
import java.util.*;

@Slf4j
public class AWRHTMLParser implements AWRParser {
    private final Map<String, Double> multipliers = new HashMap<>();

    public AWRHTMLParser() {
        multipliers.put("K", 1_000.00);
        multipliers.put("M", 1_000_000.00);
        multipliers.put("G", 1_000_000_000.00);
        multipliers.put("T", 1_000_000_000_000.00);
        multipliers.put("m", 1.0E-3);
        multipliers.put("µ", 1.0E-6);
        multipliers.put("u", 1.0E-6);
        multipliers.put("n", 1.0E-9);
    }

    @Override
    public AWRModel parse(InputStream is) throws ParsingException {
        AWRModel model = new AWRModel();
        model.setSections(new ArrayList<>());
        AWRSection currentSection = null;
        AWRSection currentSubSection = null;
        try {
            Document html = Jsoup.parse(is, "UTF-8", "");
            Element body = html.selectXpath("/html/body").first();
            String tableTitle = null;

            for (Node node : body.childNodes()) {
                if (node instanceof TextNode) {
                    TextNode text = (TextNode) node;
                    if (!text.text().trim().isEmpty()) {
                        tableTitle = text.text();
                    }
                } else if (node instanceof Element) {
                    Element element = (Element) node;
                    switch (element.tagName()) {
                    case "h1":
                    case "h2":
                        tableTitle = element.text();
                        model.getSections().add(currentSection = toSection(element.text()));
                        break;
                    case "h3":
                        tableTitle = element.text();
                        if (currentSection == null) {
                            model.getSections().add(currentSection = toSection("Unknown section"));
                        }
                        currentSection.getSubSections().add(currentSubSection = toSection(element.text()));
                        break;
                    case "table":
                        Optional.ofNullable(currentSubSection).orElse(currentSection).getTables().add(toTable(element, tableTitle));
                        tableTitle = null;
                        break;
                    }
                    element.remove();
                }
            }
            return model;
        } catch (Exception e) {
            throw new ParsingException("", e);
        }

    }

    private AWRTable toTable(Element element, String title) {
        AWRTable table = new AWRTable();
        table.setRows(new ArrayList<>());
        table.setTitle(title);
        table.setSummary(element.attr("summary"));
        table.setHeader(new ArrayList<>(0));

        for (Element rowElement : getTableRows(element)) {
            if (isHeaderRow(rowElement)) {
                table.setHeader(toHeaderCells(rowElement));
            } else {
                Row row = new Row();
                row.setCells(new ArrayList<>());
                table.getRows().add(row);
                Elements cellElements = getTableCells(rowElement);
                for (int i = 0; i < cellElements.size(); i++) {
                    Element td = cellElements.get(i);
                    String text = td.text();
                    try {
                        HeaderCell headerCell = table.getHeader().size() > i ? table.getHeader().get(i) : new HeaderCell();
                        Object value = toCellValue(text, headerCell);
                        if (headerCell.getNumeric() == null) {
                            if (value != null && !"".equals(value)) {
                                headerCell.setNumeric(value instanceof Double);
                            }
                        } else if (value != null && headerCell.getNumeric() && !(value instanceof Double)) {
                            headerCell.setNumeric(false);
                        }
                        if (headerCell.getUnits() == null || headerCell.getUnits().trim().isEmpty()) {
                            headerCell.setUnits(asUnits(text, headerCell));
                        }
                        row.getCells().add(new Cell<>(value));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                //                adjustTypesByHeaderData(table);
            }
        }

        return table;
    }

    private void adjustTypesByHeaderData(AWRTable table) {
        for (int i = 0; i < table.getHeader().size(); i++) {
            int columnIndex = i;
            try {
                HeaderCell headerCell = table.getHeader().get(columnIndex);
                if (headerCell.getNumeric() == null) {
                    continue;
                } else if (headerCell.getNumeric()) {
                    table.getRows().forEach(tableRow -> {
                        Object value = tableRow.getCells().get(columnIndex).getValue();
                        if (value == null) {
                            tableRow.getCells().set(columnIndex, new Cell<>(0D));
                        } else if (value instanceof String) {
                            logger.error("Bad cell type. Expected Double, found String. Column: {}, value: {}",
                                    headerCell.getValue(), value);
                        }
                    });
                } else {
                    table.getRows().forEach(tableRow -> {
                        Object value = tableRow.getCells().get(columnIndex);
                        if (value == null) {
                            //                                tableRow.getCells().set(columnIndex, "");
                        } else if (value instanceof Double) {
                            logger.info("Updating cell value to Text type. Column: {}, value: {}", headerCell.getValue(), value);
                            tableRow.getCells().set(columnIndex, new Cell<>(value.toString()));
                            //                                logger.error("Bad cell type. Expected Double, found String. Column: {}, value: {}", headerCell.getValue(), value);
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
        }
    }

    private Elements getTableCells(Element rowElement) {
        return rowElement.select("td");
    }

    private Elements getTableRows(Element element) {
        return element.select("tbody>tr");
    }

    private List<HeaderCell> toHeaderCells(Element rowElement) {
        Elements headers = rowElement.select(">th");
        List<HeaderCell> header = new ArrayList<>(headers.size());

        for (Element headerCell : headers) {
            HeaderCell hCell = new HeaderCell();
            hCell.setValue(headerCell.text());
            header.add(hCell);
        }

        return header;
    }

    private boolean isHeaderRow(Element rowElement) {
        return !rowElement.select(">th").isEmpty();
    }

    private Object toCellValue(String text, HeaderCell headerCell) {
        Object value;
        if (text == null || text.isEmpty()) {
            return null;
        }
        Optional<Double> asNumber = asNumber(text, headerCell);
        if (asNumber.isPresent()) {
            value = asNumber.get();
        } else {
            value = text;
        }

        return value;
    }

    private String asUnits(String text, HeaderCell headerCell) {
        return null; // TODO
    }

    private Optional<Double> asNumber(String text, HeaderCell headerCell) {
        double multiplier = 1;
        if (text == null || text.trim().isEmpty()) {
            return Optional.empty();
        }
        long dotCount = text.chars().filter(ch -> ch == '.').count();
        if (dotCount > 1) {
            return Optional.empty();
        }

        if (text.startsWith(".")) {
            text = "0" + text;
        }
        text = text.replaceAll(",", "");

        String units = null;
        String header = headerCell.getValue() != null ? headerCell.getValue() : "";
        if (header.contains("%")) {
            units = "%";
        } else if (text.endsWith("s")) {
            units = "s";
            text = text.substring(0, text.length() - 1);
        } else if (header.contains("(s)") || header.contains("(sec)")) {
            units = "s";
        }
        if (headerCell.getUnits() == null && units != null) {
            headerCell.setUnits(units);
            //            if (headerCell.getValue() == null) {
            //                headerCell.setValue(units);
            //            } else {
            //                String suffix = "(" + units + ")";
            //                if (!headerCell.getValue().endsWith(suffix)) {
            //                    headerCell.setValue(headerCell.getValue() + " " + suffix);
            //                }
            //            }
        }

        multiplier = getMultiplier(text.substring(text.length() - 1));
        if (multiplier != 1) {
            text = text.substring(0, text.length() - 1);
        }

        text = text.trim();
        try {
            return Optional.of(Double.parseDouble(text) * multiplier);
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    private double getMultiplier(String siSuffix) {
        return multipliers.getOrDefault(siSuffix, 1.00);
    }

    private Double toNumber(String text) {
        if (text == null || text.trim().isEmpty()) {
            return null;
        }
        if (text.contains(",")) {
            text = text.replaceAll(",", "");
        }
        if (text.startsWith(".")) {
            text = "0" + text;
        }
        try {
            return Double.parseDouble(text);
        } catch (Exception e) {
            return null;
        }
    }

    private AWRSection toSection(String title) {
        AWRSection section = new AWRSection();
        section.setSubSections(new ArrayList<>());
        section.setTables(new ArrayList<>());
        section.setTitle(title);
        return section;
    }
}
