package com.netikras.tools.awrdiff;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netikras.tools.awrdiff.formatter.Payload;
import com.netikras.tools.awrdiff.comparator.AWRComparator.ReportDiff;
import com.netikras.tools.awrdiff.comparator.AWRDiffFormatter;
import com.netikras.tools.awrdiff.comparator.impl.simple.SimpleAWRComparator;
import com.netikras.tools.awrdiff.formatter.impl.AWRHTMLFormatter;
import com.netikras.tools.awrdiff.model.AWRModel;
import com.netikras.tools.awrdiff.parser.impl.AWRHTMLParser;

import java.io.File;
import java.io.FileInputStream;

public class AwrEnhance {

    private String action = "print";
    private String format = "html";

    private String file1 = null;
    private String file2 = null;

    public static void main(String... args) throws Exception {
        new AwrEnhance().run(args);
    }

    private void run(String... args) throws Exception {
        if (args.length == 0) {
            printHelp();
        } else {
            for (int i = 0; i < args.length; i++) {
                switch (args[i]) {
                case "-h":
                case "--help":
                    printHelp();
                    continue;
                case "-f":
                case "--format":
                    format = args[++i];
                    continue;
                case "-a":
                case "--action":
                    action = args[++i];
                    continue;
                default:
                    if (file1 == null) {
                        file1 = args[i];
                    } else if (file2 == null) {
                        file2 = args[i];
                    } else {
                        throw new IllegalArgumentException("Unknown argument: " + args[i]);
                    }
                }
            }
        }

        Payload payload = null;
        AWRModel model1 = toModel(file1);
        switch (action) {
        case "compare":
        case "diff":
            AWRModel model2 = toModel(file2);
            if ("html".equalsIgnoreCase(format)) {
                payload = formatHTML(diff(model1, model2));
            } else if ("json".equalsIgnoreCase(format)) {
                payload = formatJSON(diff(model1, model2));
            } else {
                System.err.println("Unsupported diff format: " + format);
                System.exit(1);
            }
            break;
        case "print":
            if ("html".equalsIgnoreCase(format)) {
                payload = formatHTML(model1);
            } else if ("json".equalsIgnoreCase(format)) {
                payload = formatJSON(model1);
            } else {
                System.err.println("Unsupported output format: " + format);
                System.exit(1);
            }
            break;
        default:
            throw new IllegalArgumentException("Unsupported action: " + action);
        }

        if (payload != null) {
            System.out.write(payload.getData(), 0, payload.getData().length);
            System.out.flush();
        } else {
            System.err.println("Cannot process request");
        }
    }

    private void printHelp() {
        System.out.println(""
                + "Usage:\n"
                + "-a, --action    action to perform. Either 'print' or 'diff'/'compare'. Defaults to 'print'\n"
                + "        print <file>\n"
                + "        diff <file1> <file2>\n"
                + "-f, --format    format of the output. Either 'html' or 'json'. Defaults 'html'\n"
                + "-h, --help      print this message\n");
        System.exit(0);
    }

    private AWRModel toModel(String filePath) throws Exception {
        if (!new File(filePath).canRead()) {
            throw new IllegalAccessException("Cannot access file " + filePath);
        }
        return new AWRHTMLParser().parse(new FileInputStream(filePath));
    }

    public ReportDiff diff(AWRModel report1, AWRModel report2) {
        return new SimpleAWRComparator().compare(report1, report2);
    }

    public Payload formatJSON(AWRModel report) throws JsonProcessingException {
        return asJSON(report);
    }

    public Payload formatHTML(ReportDiff diff) {
        return new AWRDiffFormatter().format(diff);
    }

    public Payload formatHTML(AWRModel report) {
        return new AWRHTMLFormatter().format(report);
    }

    public Payload formatJSON(ReportDiff diff) throws JsonProcessingException {
        return asJSON(diff);
    }

    private Payload asJSON(Object o) throws JsonProcessingException {
        Payload payload = new Payload();
        payload.setData(new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsBytes(o));
        payload.setFormat("json");
        payload.setMime("application/json");
        return payload;
    }
}
